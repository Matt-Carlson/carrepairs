package dev.carlson.services;

import dev.carlson.data.AccountDAO;
import dev.carlson.models.Account;
import dev.carlson.models.AccountRole;
import dev.carlson.models.AuthInfo;
import io.javalin.http.UnauthorizedResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;
    @Mock
    private AccountDAO accountDAO;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAccountByIdUnauthorized(){
        AuthInfo info = mock(AuthInfo.class);
        Account account = new Account();
        account.setId(2);
        when(accountDAO.getAccountById(2)).thenReturn(account);
        when(info.getUserId()).thenReturn(5);
        when(info.getRole()).thenReturn(AccountRole.CUSTOMER);
        assertThrows(UnauthorizedResponse.class, ()->accountService.getAccountById(2, info));
    }

    @Test
    void testGetAccountByIdAuthorizedUser(){
        AuthInfo info = mock(AuthInfo.class);
        Account account = new Account();
        account.setId(4);
        when(accountDAO.getAccountById(4)).thenReturn(account);
        when(info.getUserId()).thenReturn(4);
        assertEquals(account, accountService.getAccountById(4, info));
    }

    @Test
    void testGetAccountByIdAdmin(){
        AuthInfo info = mock(AuthInfo.class);
        Account account = new Account();
        account.setId(4);
        when(accountDAO.getAccountById(4)).thenReturn(account);
        when(info.getUserId()).thenReturn(0);
        when(info.getRole()).thenReturn(AccountRole.ADMIN);
        assertEquals(account, accountService.getAccountById(4, info));
    }

    @Test
    void testUpdateAccountUnauthorized(){
        AuthInfo info = mock(AuthInfo.class);
        Account account = new Account();
        account.setId(2);
        when(info.getUserId()).thenReturn(5);
        when(info.getRole()).thenReturn(AccountRole.CUSTOMER);
        assertThrows(UnauthorizedResponse.class, ()->accountService.updateAccount(account, info));
    }

    @Test
    void testUpdateAccountAuthorizedUser(){
        AuthInfo info = mock(AuthInfo.class);
        Account account = new Account();
        account.setId(4);
        when(info.getUserId()).thenReturn(4);
        when(accountDAO.updateAccount(account)).thenReturn(true);
        assertTrue(accountService.updateAccount(account, info));
    }

    @Test
    void testUpdateAccountAdmin(){
        AuthInfo info = mock(AuthInfo.class);
        Account account = new Account();
        account.setId(4);
        when(info.getUserId()).thenReturn(0);
        when(info.getRole()).thenReturn(AccountRole.ADMIN);
        when(accountDAO.updateAccount(account)).thenReturn(true);
        assertTrue(accountService.updateAccount(account, info));}

    @Test
    void testDeleteAccountUnauthorized(){
        AuthInfo info = mock(AuthInfo.class);
        when(info.getUserId()).thenReturn(5);
        when(info.getRole()).thenReturn(AccountRole.CUSTOMER);
        assertThrows(UnauthorizedResponse.class, ()->accountService.deleteAccount(4, info));
    }

    @Test
    void testDeleteAccountAuthorizedUser(){
        AuthInfo info = mock(AuthInfo.class);
        when(info.getUserId()).thenReturn(4);
        when(accountDAO.deleteAccount(4)).thenReturn(true);
        assertTrue(accountService.deleteAccount(4, info));
    }

    @Test
    void testDeleteAccountAdmin(){
        AuthInfo info = mock(AuthInfo.class);
        when(info.getUserId()).thenReturn(0);
        when(info.getRole()).thenReturn(AccountRole.ADMIN);
        when(accountDAO.deleteAccount(4)).thenReturn(true);
        assertTrue(accountService.deleteAccount(4, info));
    }
}
