package dev.carlson.services;

import dev.carlson.data.RepairDAO;
import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import io.javalin.http.BadRequestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RepairServiceTest {

    @InjectMocks
    private RepairService repairService;

    @Mock
    private RepairDAO repairDAO;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllRepairsBadStatus(){
        assertThrows(BadRequestResponse.class,
                ()->repairService.getAllRepairs("bad", null),
                "status query only applicable for 'PENDING', 'IN_PROGRESS', or 'COMPLETED'");
    }

    @Test
    void testGetAllRepairsStatus(){
        List<Repair> list = new ArrayList<>();
        list.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(repairDAO.getAllRepairs(RepairStatus.PENDING)).thenReturn(list);
        assertEquals(list, repairService.getAllRepairs("PENDING", null));
    }

    @Test
    void testGetAllRepairsProblem(){
        List<Repair> list = new ArrayList<>();
        list.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(repairDAO.getAllRepairs("problem")).thenReturn(list);
        assertEquals(list, repairService.getAllRepairs(null, "problem"));
    }

    @Test
    void testGetAllRepairsStatusAndProblem(){
        List<Repair> list = new ArrayList<>();
        list.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(repairDAO.getAllRepairs(RepairStatus.PENDING, "problem")).thenReturn(list);
        assertEquals(list, repairService.getAllRepairs("PENDING", "problem"));
    }

    @Test
    void testGetRepairsForUserBadStatus(){
        assertThrows(BadRequestResponse.class,
                ()->repairService.getRepairsForUser(4, "bad", null),
                "status query only applicable for 'PENDING', 'IN_PROGRESS', or 'COMPLETED'");
    }

    @Test
    void testGetRepairsForUserStatus(){
        List<Repair> list = new ArrayList<>();
        list.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(repairDAO.getRepairsForUser(1, RepairStatus.PENDING)).thenReturn(list);
        assertEquals(list, repairService.getRepairsForUser(1, "PENDING", null));
    }

    @Test
    void testGetRepairsForUserProblem(){
        List<Repair> list = new ArrayList<>();
        list.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(repairDAO.getRepairsForUser(1, "problem")).thenReturn(list);
        assertEquals(list, repairService.getRepairsForUser(1, null, "problem"));
    }

    @Test
    void testGetRepairsForUserStatusAndProblem(){
        List<Repair> list = new ArrayList<>();
        list.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(repairDAO.getRepairsForUser(1, RepairStatus.PENDING, "problem")).thenReturn(list);
        assertEquals(list, repairService.getRepairsForUser(1, "PENDING", "problem"));
    }

}
