package dev.carlson.controllers;

import dev.carlson.models.AccountRole;
import dev.carlson.models.AuthInfo;
import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import dev.carlson.services.RepairService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class RepairControllerTest {
    /*
    - Mocking ctx.header() has the same inconsistency issue as ctx.pathParam(), so any tests for handleGetAllUserRepairs suffer
    - I also can't stub the static AuthController method, so they have to be tested w/ integration testing
        Expect 4/8 passing because of this
     */

    @InjectMocks
    private RepairController repairController;

    @Mock
    private RepairService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testHandleGetAllRepairs(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));
        repairs.add(new Repair(2, 4, "Dodge", "Viper", 2010, null, null, 78.50, "bad spark plug", "get new one", RepairStatus.IN_PROGRESS));
        repairs.add(new Repair(3, 7, "Chevy", "Equinox", 2010, null, null, 120.10, "problem", "solution", RepairStatus.COMPLETED));

        when(service.getAllRepairs()).thenReturn(repairs);
        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllRepairsStatus(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(service.getAllRepairs("PENDING", null)).thenReturn(repairs);
        when(context.queryParam("status")).thenReturn("PENDING");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllRepairsProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));
        repairs.add(new Repair(2, 4, "Dodge", "Viper", 2010, null, null, 78.50, "bad spark plug", "get new one", RepairStatus.IN_PROGRESS));

        when(service.getAllRepairs(null, "bad")).thenReturn(repairs);
        when(context.queryParam("problem")).thenReturn("bad");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllRepairsStatusProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(service.getAllRepairs("PENDING", "bad")).thenReturn(repairs);
        when(context.queryParam("status")).thenReturn("PENDING");
        when(context.queryParam("problem")).thenReturn("bad");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllUserRepairs(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));
        repairs.add(new Repair(2, 1, "Dodge", "Viper", 2010, null, null, 78.50, "bad spark plug", "get new one", RepairStatus.IN_PROGRESS));
        repairs.add(new Repair(3, 1, "Chevy", "Equinox", 2010, null, null, 120.10, "problem", "solution", RepairStatus.COMPLETED));

        when(service.getRepairsForUser(1)).thenReturn(repairs);
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);
        when(context.header("user-id")).thenReturn("1");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllUserRepairsStatus(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(2, 1, "Dodge", "Viper", 2010, null, null, 78.50, "bad spark plug", "get new one", RepairStatus.IN_PROGRESS));

        when(service.getRepairsForUser(1, "IN_PROGRESS", null)).thenReturn(repairs);
        when(context.queryParam("status")).thenReturn("IN_PROGRESS");
        // context.header() has the same problem as ctx.pathParam(), so tests will be inconsistent
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllUserRepairsProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));
        repairs.add(new Repair(2, 1, "Dodge", "Viper", 2010, null, null, 78.50, "bad spark plug", "get new one", RepairStatus.IN_PROGRESS));

        when(service.getRepairsForUser(1, null, "bad")).thenReturn(repairs);
        when(context.header("user-id")).thenReturn("1");
        when(context.queryParam("problem")).thenReturn("bad");
        // context.header() has the same problem as ctx.pathParam(), so tests will be inconsistent
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllUserRepairsStatusProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair(1, 1, "Ford", "F150", 2014, null, null, 12.50, "bad window motor", "replace motor", RepairStatus.PENDING));

        when(service.getRepairsForUser(1, "PENDING", "bad")).thenReturn(repairs);
        when(context.header("user-id")).thenReturn("1");
        when(context.queryParam("status")).thenReturn("PENDING");
        when(context.queryParam("problem")).thenReturn("bad");
        // context.header() has the same problem as ctx.pathParam(), so tests will be inconsistent
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }
}
