package dev.carlson.controllers;

import dev.carlson.JavalinApp;
import dev.carlson.models.Account;
import dev.carlson.models.AccountRole;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AccountControllerIntegrationTest {

    private static JavalinApp app = new JavalinApp();
    private static String adminAuthToken;
    private static String user4AuthToken;

    @BeforeAll
    public static void startService(){
        app.start(7000);
        HttpResponse<String> adminResponse = Unirest.post("http://localhost:7000/login")
                .field("email", "admin@admin")
                .field("password", "password")
                .asString();
        adminAuthToken = adminResponse.getHeaders().get("Authorization").get(0);
        HttpResponse<String> userResponse = Unirest.post("http://localhost:7000/login")
                .field("email", "ytunnow3@rediff.com")
                .field("password", "Reduced")
                .asString();
        user4AuthToken = userResponse.getHeaders().get("Authorization").get(0);
    }

    @AfterAll
    public static void stopService(){
        app.stop();
    }

    @Test
    void testHandleGetAllAccountsUnauthorized(){
        HttpResponse<String> response = Unirest.get("http://localhost:7000/accounts")
                .header("Authorization", user4AuthToken)
                .asString();
        assertAll(
                ()->assertEquals(401, response.getStatus()),
                ()->assertEquals("Unauthorized", response.getBody())
        );
    }

    @Test
    void testHandleGetAllAccountsAuthorized(){
        HttpResponse<List<Account>> response = Unirest.get("http://localhost:7000/accounts")
                .header("Authorization", adminAuthToken)
                .asObject(new GenericType<List<Account>>() {});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }

    @Test
    void testHandleGetAccountByIdUnauthorized(){
        HttpResponse<String> response = Unirest.get("http://localhost:7000/accounts/6")
                .header("Authorization", user4AuthToken)
                .asString();
        assertAll(
                ()->assertEquals(401, response.getStatus()),
                ()->assertEquals("Unauthorized", response.getBody())
        );
    }

    @Test
    void testHandleGetAccountByIdAuthorized(){
        HttpResponse<Account> response = Unirest.get("http://localhost:7000/accounts/4")
                .header("Authorization", user4AuthToken)
                .asObject(Account.class);
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertEquals(4, response.getBody().getId())
        );
    }

    @Test
    void testHandleGetAccountByIdAdmin(){
        HttpResponse<Account> response = Unirest.get("http://localhost:7000/accounts/4")
                .header("Authorization", adminAuthToken)
                .asObject(Account.class);
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertEquals(4, response.getBody().getId())
        );
    }

    @Test
    void testHandleUpdateAccountUnauthorized(){
        Account account = makeAccountForUser4();
        HttpResponse<String> response = Unirest.put("http://localhost:7000/accounts/6")
                .header("Authorization", user4AuthToken)
                .body(JavalinJson.toJson(account))
                .asString();
        assertAll(
                ()->assertEquals(401, response.getStatus()),
                ()->assertEquals("Unauthorized", response.getBody())
        );
    }

    @Test
    void testHandleUpdateAccountAuthorized(){
        Account account = makeAccountForUser4();
        account.setFirstName("Yen");
        HttpResponse<List<Account>> response = Unirest.put("http://localhost:7000/accounts/4")
                .header("Authorization", user4AuthToken)
                .body(JavalinJson.toJson(account))
                .asObject(new GenericType<List<Account>>() {});
        assertEquals(201, response.getStatus());
    }

    @Test
    void testHandleUpdateAccountAdmin(){
        Account account = makeAccountForUser4();
        HttpResponse<List<Account>> response = Unirest.put("http://localhost:7000/accounts/4")
                .header("Authorization", adminAuthToken)
                .body(JavalinJson.toJson(account))
                .asObject(new GenericType<List<Account>>() {});
        assertEquals(201, response.getStatus());
    }

    private Account makeAccountForUser4(){
        Account account = new Account();
        account.setId(4);
        account.setFirstName("Yetti");
        account.setLastName("Tunnow");
        account.setEmail("ytunnow3@rediff.com");
        account.setPassword("Reduced");
        account.setRole(AccountRole.CUSTOMER);
        return account;
    }

}
