package dev.carlson.controllers;

import dev.carlson.models.Account;
import dev.carlson.models.AuthInfo;
import dev.carlson.services.AccountService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountController {
    private final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private AccountService service = new AccountService();
    private static final String AUTH_HEADER = "Authorization";

    public void handleGetAllAccounts(Context ctx){
        ctx.json(service.getAllAccounts());
    }

    public void handleGetAccountById(Context ctx){
        String idString = ctx.pathParam("id");

        if(idString.matches("^\\d+$")) {
            String token = ctx.header(AUTH_HEADER);
            AuthInfo info = AuthController.getUserInfoFromToken(token);
            Account account = service.getAccountById(Integer.parseInt(idString), info);
            if (account!=null) {
                ctx.json(account);
                logger.info("returned account {}", account.getId());
            }
        } else {
            logBadIdParamResponse(idString);
        }
    }

    public void handlePostAccount(Context ctx){
        Account account = ctx.bodyAsClass(Account.class);
        service.addAccount(account);
        ctx.status(201);
    }

    public void handleUpdateAccount(Context ctx){
        Account updatedAccount = ctx.bodyAsClass(Account.class);
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            String token = ctx.header(AUTH_HEADER);
            AuthInfo info = AuthController.getUserInfoFromToken(token);
            // ensure id in object matches the URI
            updatedAccount.setId(Integer.parseInt(idString));
            service.updateAccount(updatedAccount, info);
            logger.info("Account {} updated by user {}", updatedAccount.getId(), info.getUserId());
            ctx.status(201);
        } else {
            logBadIdParamResponse(idString);
        }
    }

    public void handleDeleteAccount(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            String token = ctx.header(AUTH_HEADER);
            AuthInfo info = AuthController.getUserInfoFromToken(token);
            service.deleteAccount(Integer.parseInt(idString), info);
            logger.info("Account {} deleted by user {}", idString, info.getUserId());
            ctx.status(200);
        } else {
            logBadIdParamResponse(idString);
        }
    }

    private void logBadIdParamResponse(String idString){
        logger.warn("GET account by id unable to parse \"{}\" as int", idString);
        throw new BadRequestResponse(String.format("Unable to parse \"%s\" as an int", idString));
    }
}
