package dev.carlson.controllers;

import dev.carlson.models.AuthInfo;
import dev.carlson.models.Repair;
import dev.carlson.services.RepairService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RepairController {
    Logger logger = LoggerFactory.getLogger(RepairController.class);
    private RepairService service = new RepairService();

    public void handleGetAllRepairs(Context ctx) {
        // Handle URI query params
        String statusParamString = ctx.queryParam("status");
        String problemParamString = ctx.queryParam("problem");

        // return all repairs for admin
        if (statusParamString != null || problemParamString != null) {
            ctx.json(service.getAllRepairs(statusParamString, problemParamString));
            logger.info("returned query param filtered repairs");
        } else {
            ctx.json(service.getAllRepairs());
            logger.info("returned all repairs");
        }
    }

    public void handleGetAllUserRepairs(Context ctx) {
        AuthInfo info = AuthController.getUserInfoFromToken(ctx.header("Authorization"));

        // Handle URI query params
        String statusParamString = ctx.queryParam("status");
        String problemParamString = ctx.queryParam("problem");

        // return only current users repairs
        if (statusParamString != null || problemParamString != null) {
            ctx.json(service.getRepairsForUser(info.getUserId(), statusParamString, problemParamString));
            logger.info("returned query param filtered repairs for user {}", info.getUserId());
        } else {
            ctx.json(service.getRepairsForUser(info.getUserId()));
            logger.info("returned all repairs for user {}", info.getUserId());
        }
    }

    public void handleGetRepairById(Context ctx) {
        String idString = ctx.pathParam("id");
        if (idString.matches("^\\d+$")) {
            int idNum = Integer.parseInt(idString);
            Repair repair = service.getRepairById(idNum);
            if (repair!=null) {
                ctx.json(repair);
                logger.info("returned repair {}", repair.getId());
            }
        } else {
            logger.warn("GET repair by id unable to parse \"{}\" as int", idString);
            throw new BadRequestResponse(String.format("Unable to parse \"%s\" as an int", idString));
        }
    }

    public void handleGetUserRepairById(Context ctx) {
        String idString = ctx.pathParam("id");
        AuthInfo info = AuthController.getUserInfoFromToken(ctx.header("Authorization"));
        if (idString.matches("^\\d+$")) {
            int idNum = Integer.parseInt(idString);

            // retrieve repair, ensure it belongs to the current user
            Repair repair = service.getRepairById(idNum);
            if (repair != null) {
                if (repair.getClientId() == info.getUserId()) {
                    ctx.json(repair);
                    logger.info("returned repair {}", repair.getId());
                } else {
                    logger.warn("Attempt to view repair for client {} by user {}", repair.getClientId(), info.getUserId());
                    throw new UnauthorizedResponse();
                }
            }
        } else {
            logger.warn("GET repair by id unable to parse \"{}\" as int", idString);
            throw new BadRequestResponse(String.format("Unable to parse \"%s\" as an int", idString));
        }
    }

    public void handleCreateRepair(Context ctx) {
            Repair repair = ctx.bodyAsClass(Repair.class);
            service.addRepair(repair);
            ctx.status(201);
    }

    public void handleUpdateRepair(Context ctx) {
        Repair repair = ctx.bodyAsClass(Repair.class);
        String repairIdString = ctx.pathParam("id");
        if (repairIdString.matches("^\\d+$")) {
            int repairId = Integer.parseInt(repairIdString);
                // ensure ID correlates to URI
                repair.setId(repairId);
                service.updateRepair(repair);
                ctx.status(201);
        } else {
            logger.warn("Unable to parse \"{}\" as int", repairIdString);
            throw new BadRequestResponse(String.format("Unable to parse \"%s\" as an int", repairIdString));
        }
    }

    public void handleDeleteRepair(Context ctx) {
        String repairIdString = ctx.pathParam("id");
        if (repairIdString.matches("^\\d+$")) {
            int repairId = Integer.parseInt(repairIdString);
                service.deleteRepair(repairId);
        } else {
            logger.warn("Unable to parse \"{}\" as int", repairIdString);
            throw new BadRequestResponse(String.format("Unable to parse \"%s\" as an int", repairIdString));
        }
    }
}
