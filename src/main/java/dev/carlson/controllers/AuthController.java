package dev.carlson.controllers;


import dev.carlson.managers.JwtTokenManager;
import dev.carlson.models.Account;
import dev.carlson.models.AuthInfo;
import dev.carlson.services.AccountService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {
    private final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private final AccountService accountService = new AccountService();
    private static final String AUTH_HEADER = "Authorization";
    // multiple classes need specific user id info from the token. Static to ensure consistent treatment of JWTs
    private static final JwtTokenManager tokenManager = new JwtTokenManager();

    // static method for other handlers to get the user id and role from the token
    // There's probably a better way to do this but idk how. (parse the token only once, not twice)
    public static AuthInfo getUserInfoFromToken(String token){
        return tokenManager.getUserInfoFromToken(token);
    }

    
    public void authenticateLogin(Context ctx){
        String email = ctx.formParam("email");
        String pass = ctx.formParam("password");
        logger.info("{} attempted login", email);
        Account account = accountService.getAccountByEmail(email);
        if(account!=null && account.getPassword().equals(pass)){
            logger.info("successful login");
            ctx.header(AUTH_HEADER, tokenManager.issueToken(String.valueOf(account.getId()), account.getRole()));
            ctx.status(200);
            return;
        }
        logger.warn("failed login attempt");
        throw new UnauthorizedResponse("Credentials were incorrect");
    }

    public void authorizeToken(Context ctx) {
        String token = ctx.header(AUTH_HEADER);

        if(tokenManager.authorizeUser(token)){
            logger.info("User authenticated");
            return;
        }
        logger.warn("JWT authorization failed");
        throw new UnauthorizedResponse();
    }

    public void authorizeAdminToken(Context ctx){
        String token = ctx.header(AUTH_HEADER);

        if(tokenManager.authorizeAdmin(token)){
            logger.info("Admin authenticated");
            return;
        }
        logger.warn("Admin authorization failed");
        throw new UnauthorizedResponse();
    }
}
