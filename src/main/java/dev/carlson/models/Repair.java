package dev.carlson.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class Repair implements Serializable {

    private int id;
    private int clientId;
    private String make;
    private String model;
    private int year;
    private Timestamp dateSubmitted;
    private Timestamp dateCompleted;
    private double price;
    private String problem;
    private String solution;
    private RepairStatus status;

    public Repair(){}

    public Repair(int id, int clientId, String make, String model, int year, Timestamp dateSubmitted, Timestamp dateCompleted, double price, String problem, String solution, RepairStatus status) {
        this.id = id;
        this.clientId = clientId;
        this.make = make;
        this.model = model;
        this.year = year;
        this.dateSubmitted = dateSubmitted;
        this.dateCompleted = dateCompleted;
        this.price = price;
        this.problem = problem;
        this.solution = solution;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Timestamp getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Timestamp dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public Timestamp getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Timestamp dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Repair repair = (Repair) o;
        return id == repair.id && clientId == repair.clientId && year == repair.year && Double.compare(repair.price, price) == 0 && Objects.equals(make, repair.make) && Objects.equals(model, repair.model) && Objects.equals(dateSubmitted, repair.dateSubmitted) && Objects.equals(dateCompleted, repair.dateCompleted) && Objects.equals(problem, repair.problem) && Objects.equals(solution, repair.solution) && status == repair.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, make, model, year, dateSubmitted, dateCompleted, price, problem, solution, status);
    }

    @Override
    public String toString() {
        return "Repair{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", dateSubmitted=" + dateSubmitted +
                ", dateCompleted=" + dateCompleted +
                ", price=" + price +
                ", problem='" + problem + '\'' +
                ", solution='" + solution + '\'' +
                ", status=" + status +
                '}';
    }
}
