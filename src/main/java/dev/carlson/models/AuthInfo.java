package dev.carlson.models;

import java.sql.Timestamp;

public class AuthInfo {
    private AccountRole role;
    private int userId;
    private Timestamp exp;

    public AuthInfo() {
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Timestamp getExp() {
        return exp;
    }

    public void setExp(Timestamp exp) {
        this.exp = exp;
    }
}
