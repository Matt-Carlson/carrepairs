package dev.carlson;

public class Driver {
    public static void main(String[] args) {
        /*
        Things that can be added
        - full unit and integration testing
        - password hashing
         */
        JavalinApp javalinApp = new JavalinApp();
        javalinApp.start(7000);
    }
}
