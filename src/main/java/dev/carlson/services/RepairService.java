package dev.carlson.services;

import dev.carlson.data.RepairDAO;
import dev.carlson.data.RepairDAOImpl;
import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import io.javalin.http.BadRequestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

public class RepairService {
    private final Logger logger = LoggerFactory.getLogger(RepairService.class);
    private RepairDAO repairDAO = new RepairDAOImpl();

    public List<Repair> getAllRepairs(){
        return repairDAO.getAllRepairs();
    }

    public List<Repair> getAllRepairs(String statusString, String problem){
        // Turn string into enum, ensure .valueof() won't throw exception
        RepairStatus status = null;
        for (RepairStatus s : RepairStatus.values()){
            if (s.toString().equals(statusString)){
                status = RepairStatus.valueOf(statusString);
            }
        }
        if (statusString!=null && status==null){
            // invalid status param
            logger.warn("Bad status query param '{}'", statusString);
            throw new BadRequestResponse("status query only applicable for 'PENDING', 'IN_PROGRESS', or 'COMPLETED'");
        }

        if (status != null){
            if (problem!=null){
                // both
                return repairDAO.getAllRepairs(status, problem);
            } else {
                // only status
                return repairDAO.getAllRepairs(status);
            }
        } else {
            // only problem
            return repairDAO.getAllRepairs(problem);
        }
    }

    public List<Repair> getRepairsForUser(int userId){
        return repairDAO.getRepairsForUser(userId);
    }

    public List<Repair> getRepairsForUser(int id, String statusString, String problem) {
        // Turn string into enum, ensure .valueof() won't throw exception
        RepairStatus status = null;
        for (RepairStatus s : RepairStatus.values()){
            if (s.toString().equals(statusString)){
                status = RepairStatus.valueOf(statusString);
            }
        }
        if (statusString!=null && status==null){
            // invalid status param
            logger.warn("Bad status query param '{}'", statusString);
            throw new BadRequestResponse("status query only applicable for 'PENDING', 'IN_PROGRESS', or 'COMPLETED'");
        }

        if (status != null){
            if (problem!=null){
                // both
                return repairDAO.getRepairsForUser(id, status, problem);
            } else {
                // only status
                return repairDAO.getRepairsForUser(id, status);
            }
        } else {
            // only problem
            return repairDAO.getRepairsForUser(id, problem);
        }
    }

    public Repair getRepairById(int id){
        return repairDAO.getRepairById(id);
    }

    public boolean addRepair(Repair repair){
        // set the date submitted to today, completed to null (update when finished)
        repair.setDateSubmitted(new Timestamp(Calendar.getInstance().getTime().getTime()));
        repair.setDateCompleted(null);
        return repairDAO.addRepair(repair);
    }

    public boolean updateRepair(Repair repair){
        return repairDAO.updateRepair(repair);
    }

    public boolean deleteRepair(int id){
        return repairDAO.deleteRepair(id);
    }

}
