package dev.carlson.services;

import dev.carlson.data.AccountDAO;
import dev.carlson.data.AccountDAOImpl;
import dev.carlson.models.Account;
import dev.carlson.models.AccountRole;
import dev.carlson.models.AuthInfo;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AccountService {
    private AccountDAO accountDao = new AccountDAOImpl();
    private final Logger logger = LoggerFactory.getLogger(AccountService.class);

    public List<Account> getAllAccounts(){
        return accountDao.getAllAccounts();
    }

    public Account getAccountById(int id, AuthInfo info){
        Account account = accountDao.getAccountById(id);
        if(account==null)
            return null;
        if (account.getId()==info.getUserId() || info.getRole().equals(AccountRole.ADMIN)) {
            return account;
        }
        logger.warn("Attempt to view account {} by user {}", account.getId(), info.getUserId());
        throw new UnauthorizedResponse();
    }

    // for login functionality
    public Account getAccountByEmail(String email){
        return accountDao.getAccountByEmail(email);
    }

    public boolean addAccount(Account account){
        // make users admin in DB.
        account.setRole(AccountRole.CUSTOMER);
        return accountDao.addAccount(account);
    }

    public boolean updateAccount(Account account, AuthInfo info){
        if (account.getId()==info.getUserId() || info.getRole().equals(AccountRole.ADMIN)) {
            return accountDao.updateAccount(account);
        }
        logger.warn("Attempt to alter account {} by user {}", account.getId(), info.getUserId());
        throw new UnauthorizedResponse();
    }

    public boolean deleteAccount(int id, AuthInfo info){
        if (id==info.getUserId() || info.getRole().equals(AccountRole.ADMIN)) {
            return accountDao.deleteAccount(id);
        }
        logger.warn("Attempt to delete account {} by user {}", id, info.getUserId());
        throw new UnauthorizedResponse();
    }
}
