package dev.carlson.data;

import dev.carlson.models.Account;
import dev.carlson.models.AccountRole;
import dev.carlson.utilities.ConnectionUtil;
import io.javalin.http.InternalServerErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDAOImpl implements AccountDAO{
    private final Logger logger = LoggerFactory.getLogger(AccountDAOImpl.class);


    @Override
    public List<Account> getAllAccounts() {
        List<Account> accounts = new ArrayList<>();
        try(Connection connection = ConnectionUtil.getConnection();
            Statement statement = connection.createStatement()){

            ResultSet rs = statement.executeQuery("select * from account");
            while(rs.next()){
                Account account = createAccountObject(rs);
                accounts.add(account);
            }
            logger.info("Retrieved all accounts from db");
        } catch (SQLException e) {
            logger.error("{} : {}", e.getClass(), e.getMessage());
            throw new InternalServerErrorResponse();
        }
        return accounts;
    }

    @Override
    public Account getAccountById(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement("select * from account where id = ?")){
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                Account account = createAccountObject(rs);
                logger.info("Retrieved account {} from db", account.getId());
                return account;
            }
        } catch (SQLException e) {
            logger.error("{} : {}", e.getClass(), e.getMessage());
            throw new InternalServerErrorResponse();
        }
        logger.warn("no account found for id {}", id);
        return null;
    }

    @Override
    public Account getAccountByEmail(String email) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement("select * from account where email = ?")){
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                Account account = createAccountObject(rs);
                logger.info("Retrieved account {} from db", account.getId());
                return account;
            }
            logger.info("Retrieved account from db");
        } catch (SQLException e) {
            logger.error("{} : {}", e.getClass(), e.getMessage());
            throw new InternalServerErrorResponse();
        }
        logger.warn("no account found for email {}", email);
        return null;
    }

    @Override
    public boolean addAccount(Account account) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement("insert into account values(default,?,?,?,?,?)")){
            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setString(3, account.getEmail());
            statement.setString(4, account.getPassword());
            statement.setString(5, AccountRole.CUSTOMER.toString());

            statement.executeUpdate();
            logger.info("Added new account to db");
            return true;
        } catch (SQLException e) {
            logger.error("{} : {}", e.getClass(), e.getMessage());
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public boolean updateAccount(Account account) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement("update account set first_name=?,last_name=?,email=?,password=? where id=?")){
            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setString(3, account.getEmail());
            statement.setString(4, account.getPassword());
            statement.setInt(5, account.getId());

            statement.executeUpdate();
            logger.info("Updated account {} in db", account.getId());
            return true;
        } catch (SQLException e) {
            logger.error("{} : {}", e.getClass(), e.getMessage());
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public boolean deleteAccount(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement("delete from account where id=?")){
            statement.setInt(1, id);

            statement.executeUpdate();
            logger.info("Deleted account {} in db", id);
            return true;
        } catch (SQLException e) {
            logger.error("{} : {}", e.getClass(), e.getMessage());
            throw new InternalServerErrorResponse();
        }
    }

    /**
     * Creates account object. Reduces copy/pasted code
     * @param rs
     * @return Account object that represents what the given ResultSet currently refers to
     * @throws SQLException
     */
    private Account createAccountObject(ResultSet rs) throws SQLException {
        Account account = new Account();
        account.setId(rs.getInt("id"));
        account.setFirstName(rs.getString("first_name"));
        account.setLastName(rs.getString("last_name"));
        account.setEmail(rs.getString("email"));
        account.setPassword(rs.getString("password"));
        String roleString = rs.getString("user_role");
        switch (roleString){
            case("CUSTOMER"):
                account.setRole(AccountRole.CUSTOMER);
                break;
            case("ADMIN"):
                account.setRole(AccountRole.ADMIN);
                break;
            default:
                account.setRole(null);
        }
        return account;
    }
}
