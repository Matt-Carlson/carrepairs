package dev.carlson.data;

import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import dev.carlson.utilities.ConnectionUtil;
import io.javalin.http.InternalServerErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RepairDAOImpl implements RepairDAO {
    private final Logger logger = LoggerFactory.getLogger(RepairDAOImpl.class);


    @Override
    public List<Repair> getAllRepairs() {
        List<Repair> repairs = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection();
             Statement stmt = connection.createStatement()) {

            ResultSet rs = stmt.executeQuery("select * from repair order by date_submitted desc");

            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs from db");
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getAllRepairs(RepairStatus status) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where status=? order by date_submitted desc")) {

            statement.setString(1, status.toString());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs with status {}", status);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getAllRepairs(String problem) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where problem like ? order by date_submitted desc")) {

            statement.setString(1, '%'+problem+'%');
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs with problem containing '{}'", problem);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getAllRepairs(RepairStatus status, String problem) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where status=? and problem like ? order by date_submitted desc")) {

            statement.setString(1, status.toString());
            statement.setString(2, '%'+problem+'%');
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs with status {} and problem containing '{}'", status, problem);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getRepairsForUser(int userId) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where client_id = ? order by date_submitted desc")) {

            statement.setInt(1, userId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs for user {} from db", userId);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getRepairsForUser(int userId, RepairStatus status) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where status=? and client_id=? order by date_submitted desc")) {

            statement.setString(1, status.toString());
            statement.setInt(2, userId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs for user {} with status {}", userId, status);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getRepairsForUser(int userId, String problem) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where client_id=? and problem like ? order by date_submitted desc")) {

            statement.setInt(1, userId);
            statement.setString(2, '%'+problem+'%');
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs for user {} with problem containing '{}'", userId, problem);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public List<Repair> getRepairsForUser(int userId, RepairStatus status, String problem) {
        List<Repair> repairs = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where client_id=? and status=? and problem like ? order by date_submitted desc")) {

            statement.setInt(1, userId);
            statement.setString(2, status.toString());
            statement.setString(3, '%'+problem+'%');
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Repair repair = createRepairObject(rs);
                repairs.add(repair);
            }
            logger.info("Retrieved all repairs for client {} with status {} and problem containing '{}'", userId, status, problem);
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        return repairs;
    }

    @Override
    public Repair getRepairById(int id) {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from repair where id = ?")) {

            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                logger.info("Retrieved repair with id {} from db", id);
                return createRepairObject(rs);
            }
        } catch (SQLException e) {
            sqlExceptionResponse(e);
        }
        logger.warn("Unable to find repair with id {}", id);
        return null;
    }

    @Override
    public boolean addRepair(Repair repair) {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("insert into repair values(default,?,?,?,?,?,?,?,?,?,?)")) {

            statement.setInt(1, repair.getClientId());
            statement.setString(2, repair.getMake());
            statement.setString(3, repair.getModel());
            statement.setInt(4, repair.getYear());
            statement.setTimestamp(5, repair.getDateSubmitted());
            statement.setTimestamp(6, repair.getDateCompleted());
            statement.setDouble(7, repair.getPrice());
            statement.setString(8, repair.getProblem());
            statement.setString(9, repair.getSolution());
            statement.setString(10, repair.getStatus().toString());

            statement.executeUpdate();
            logger.info("Added a new repair to the db");
            return true;
        } catch (SQLException e) {
            sqlExceptionResponse(e);
            return false;
        }
    }

    @Override
    public boolean updateRepair(Repair repair) {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("update repair set client_id=?,make=?,model=?,year=?,date_submitted=?,date_completed=?,price=?,problem=?,solution=?,status=? where id=?")) {

            statement.setInt(1, repair.getClientId());
            statement.setString(2, repair.getMake());
            statement.setString(3, repair.getModel());
            statement.setInt(4, repair.getYear());
            statement.setTimestamp(5, repair.getDateSubmitted());
            statement.setTimestamp(6, repair.getDateCompleted());
            statement.setDouble(7, repair.getPrice());
            statement.setString(8, repair.getProblem());
            statement.setString(9, repair.getSolution());
            statement.setString(10, repair.getStatus().toString());
            statement.setInt(11, repair.getId());

            statement.executeUpdate();
            logger.info("Updated repair {}", repair.getId());
            return true;
        } catch (SQLException e) {
            sqlExceptionResponse(e);
            return false;
        }
    }

    @Override
    public boolean deleteRepair(int id) {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement("delete from repair where id = ?")) {

            statement.setInt(1, id);
            statement.executeUpdate();
            logger.info("Deleted repair {} from the db", id);
            return true;
        } catch (SQLException e) {
            sqlExceptionResponse(e);
            return false;
        }
    }

    private void sqlExceptionResponse(Exception e){
        logger.error("{} : {}", e.getClass(), e.getMessage());
        throw new InternalServerErrorResponse();
    }

    /**
     * Creates a repair object from the ResultSet
     * @param rs
     * @return Repair object that represents what the given ResultSet currently refers to
     * @throws SQLException
     */
    private Repair createRepairObject(ResultSet rs) throws SQLException {
        Repair repair = new Repair();
        repair.setId(rs.getInt("id"));
        repair.setClientId(rs.getInt("client_id"));
        repair.setMake(rs.getString("make"));
        repair.setModel(rs.getString("model"));
        repair.setYear(rs.getInt("year"));
        repair.setDateSubmitted(rs.getTimestamp("date_submitted"));
        repair.setDateCompleted(rs.getTimestamp("date_completed"));
        repair.setPrice(rs.getDouble("price"));
        repair.setProblem(rs.getString("problem"));
        repair.setSolution(rs.getString("solution"));
        String statusString = rs.getString("status");
        switch (statusString){
            case("PENDING"):
                repair.setStatus(RepairStatus.PENDING);
                break;
            case("IN_PROGRESS"):
                repair.setStatus(RepairStatus.IN_PROGRESS);
                break;
            case("COMPLETED"):
                repair.setStatus(RepairStatus.COMPLETED);
                break;
            default:
                repair.setStatus(null);
        }
        return repair;
    }
}
