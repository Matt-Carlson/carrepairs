package dev.carlson.data;

import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import dev.carlson.services.RepairService;

import java.util.List;

public interface RepairDAO {
    public List<Repair> getAllRepairs();
    public List<Repair> getAllRepairs(RepairStatus status);
    public List<Repair> getAllRepairs(String problem);
    public List<Repair> getAllRepairs(RepairStatus status, String problem);
    public List<Repair> getRepairsForUser(int userId);
    public List<Repair> getRepairsForUser(int userId, RepairStatus status);
    public List<Repair> getRepairsForUser(int userId, String problem);
    public List<Repair> getRepairsForUser(int userId, RepairStatus status, String problem);
    public Repair getRepairById(int id);

    public boolean addRepair(Repair repair);
    public boolean updateRepair(Repair repair);
    public boolean deleteRepair(int id);
}
