package dev.carlson.data;

import dev.carlson.models.Account;

import java.util.List;

public interface AccountDAO {
    public List<Account> getAllAccounts();
    public Account getAccountById(int id);
    public Account getAccountByEmail(String user);

    public boolean addAccount(Account account);
    public boolean updateAccount(Account account);
    public boolean deleteAccount(int id);
}
