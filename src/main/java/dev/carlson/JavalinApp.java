package dev.carlson;

import dev.carlson.controllers.AccountController;
import dev.carlson.controllers.AuthController;
import dev.carlson.controllers.RepairController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    RepairController repairController = new RepairController();
    AccountController accountController = new AccountController();
    AuthController authController = new AuthController();

    Javalin app = Javalin.create().routes(() -> {
        path("repairs", () -> {
            before("/", authController::authorizeToken);
            get(repairController::handleGetAllUserRepairs);
            path(":id", () -> {
                before("/", authController::authorizeToken);
                get(repairController::handleGetUserRepairById);
            });
        });
        path("all-repairs", () -> {
            before("/", authController::authorizeAdminToken);
            get(repairController::handleGetAllRepairs);
            post(repairController::handleCreateRepair);
            path(":id", () -> {
                before("/", authController::authorizeAdminToken);
                get(repairController::handleGetRepairById);
                delete(repairController::handleDeleteRepair);
                put(repairController::handleUpdateRepair);
            });
        });
        path("accounts", () -> {
            before("/", authController::authorizeAdminToken);
            get(accountController::handleGetAllAccounts);
            path(":id", () -> {
                before("/", authController::authorizeToken);
                get(accountController::handleGetAccountById);
                put(accountController::handleUpdateAccount);
                delete(accountController::handleDeleteAccount);
            });
        });
        path("login", () -> {
            post(authController::authenticateLogin);
        });
        path("create-account", () -> {
            post(accountController::handlePostAccount);
        });
    });

    public void start(int port){
        this.app.start(port);
    }

    public void stop(){
        this.app.stop();
    }

}
