package dev.carlson.managers;

import dev.carlson.models.AccountRole;
import dev.carlson.models.AuthInfo;
import io.javalin.http.UnauthorizedResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;
import java.time.ZonedDateTime;
import java.util.Date;

public class JwtTokenManager {

    private final Key key;
    private final Logger logger = LoggerFactory.getLogger(JwtTokenManager.class);
    private static final String JWT_EXCEPTION_RESPONSE = "JWT malformed or expired";

    public JwtTokenManager(){
        key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    public String issueToken(String userId, AccountRole role){
        return Jwts.builder()
                .setSubject(userId)
                .claim("role", role.toString())
                .setExpiration(Date.from(ZonedDateTime.now().plusHours(3).toInstant()))
                .signWith(key).compact();
    }

    // utility to read information from the JWT elsewhere
    public AuthInfo getUserInfoFromToken(String token){
        try {
            Claims c = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
            AuthInfo info = new AuthInfo();
            info.setUserId(Integer.parseInt(c.getSubject()));
            String roleString = c.get("role", String.class);
            switch (roleString){
                case("CUSTOMER"):
                    info.setRole(AccountRole.CUSTOMER);
                    break;
                case("ADMIN"):
                    info.setRole(AccountRole.ADMIN);
                    break;
                default:
                    info.setRole(null);
            }
            return info;
        } catch (JwtException e) {
            logger.warn(JWT_EXCEPTION_RESPONSE);
            throw new UnauthorizedResponse();
        }
    }

    public boolean authorizeAdmin(String token){
        try {
            Claims c = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
            String role = c.get("role", String.class);
            Date exp = c.getExpiration();

            return (role.equals("ADMIN") && exp.after(Date.from(ZonedDateTime.now().toInstant())));
        } catch (Exception e){
            logger.warn(JWT_EXCEPTION_RESPONSE);
            throw new UnauthorizedResponse();
        }
    }

    public boolean authorizeUser(String token){
        try {
            Claims claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
            Date exp = claims.getExpiration();
            return (exp.after(Date.from(ZonedDateTime.now().toInstant())));
        } catch (Exception e){
            logger.warn(JWT_EXCEPTION_RESPONSE);
            throw new UnauthorizedResponse();
        }
    }
}
