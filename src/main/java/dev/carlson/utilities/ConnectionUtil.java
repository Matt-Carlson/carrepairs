package dev.carlson.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    public static synchronized Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            String url = System.getenv("p0_connectionUrl");
            String username = System.getenv("p0_username");
            String password = System.getenv("p0_password");
            connection = DriverManager.getConnection(url, username, password);
        }
        return connection;
    }

}
